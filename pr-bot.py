#!/usr/bin/env python3

import argparse
import requests
from git import Repo
from git.exc import GitCommandError
import re
import sys


class PrMirror(object):
    def __init__(self, path, trusted_users, push_url=None):
        self.repo = Repo(path)
        self.fetch_open_pull_requests()
        print(f"trusted_users: {trusted_users}")
        self.trusted_users = trusted_users
        self.push_url = push_url
        self.keep_prs = set()

    def fetch_open_pull_requests(self):
        url = "https://api.github.com/repos/bigbluebutton/bigbluebutton/pulls?state=open"
        r = requests.get(url, headers={'Accept': 'application/vnd.github.v3+json'})
        self.pr_data = { pr['number']:pr for pr in r.json() }

    def pr_detail(self, number):
        pr = self.pr_data[number]
        remote_url = pr['head']['repo']['clone_url']
        ref = pr['head']['ref']
        print(f"PR: {number} {remote_url} {ref}")

    def sync_trusted(self):
        for number in self.pr_data:
            pr = self.pr_data[number]
            owner = pr['head']['repo']['owner']['login']
            if owner in self.trusted_users:
                self.sync_pr(number)
                self.keep_prs.add(number)

    def cleanup(self):
        available_prs = set()
        reg = re.compile(r'^pr-(\d+)$')
        for head in self.repo.heads:
            md = reg.match(head.name)
            if md:
                available_prs.add(int(md[1]))

        for remote in self.repo.remotes:
            md = reg.match(remote.name)
            if md:
                available_prs.add(int(md[1]))
        for number in (available_prs - self.keep_prs):
            self.purge_pr(number)

    def sync_pr(self, number):
        if self.push_url is None:
            raise RuntimeError("Cannot Sync without push url")
        print(f"syncing {number}")
        pr = Pr(self.repo, self.pr_data[number], self.push_url)
        pr.pull()
        pr.push()

    def purge_pr(self, number):
        pr = Pr(self.repo, self.pr_data.get(number, {'number': number}), self.push_url)
        pr.purge()

    def sync_branch(self, name):
        if name not in self.repo.heads:
            return
        branch = self.repo.heads[name]
        branch.checkout()
        origin = self.repo.remotes[branch.tracking_branch().remote_name]
        origin.set_url(self.push_url, push=True)
        origin.pull()
        origin.push()


class Pr(object):
    def __init__(self, repo, pr, push_url):
        self.repo = repo
        self.pr = pr
        self.push_url = push_url

    @property
    def remote(self):
        return f"pr-{self.pr['number']}"

    @property
    def remote_url(self):
        return self.pr['head']['repo']['clone_url']

    @property
    def branch(self):
        return f"pr-{self.pr['number']}"

    def purge(self):
        if self.remote in self.repo.remotes:
            try:
                self.repo.remotes[self.remote].push(self.branch, delete=True)
            except GitCommandError:
                # catch if remote does not have this branch
                pass
            self.repo.delete_remote(self.remote)
        if self.branch in self.repo.heads:
            self.repo.delete_head(self.branch, force=True)

    def pull(self):
        if self.remote not in self.repo.remotes:
            self.repo.create_remote(self.remote, self.remote_url)
            self.repo.remotes[self.remote].set_url(self.push_url, push=True)
        self.repo.remotes[self.remote].fetch()
        
        origin = self.repo.remotes[self.remote]
        if self.branch not in self.repo.heads:
            self.repo.create_head(self.branch, origin.refs[self.pr['head']['ref']])
            self.repo.heads[self.branch].set_tracking_branch(origin.refs[self.pr['head']['ref']])
        self.repo.heads[self.branch].checkout()
        origin.pull()

    def push(self):
        self.repo.remotes[self.remote].push(self.branch)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Synchronize BBB Pull Requests to another repo (at our own GitLab instance). This allows you to trigger builds from Pull Requests in GitLab.')
    parser.add_argument('repo_dir', default='.', help='Path to the directory where you keep your local BBB repo')
    parser.add_argument('push_url', help='The Git URL which the branches will be pushed to')
    parser.add_argument('trusted_users', nargs='+', help='Specify users whose Pull Requests may be pushed to your repo')
    args = parser.parse_args()
    m = PrMirror(args.repo_dir, args.trusted_users, args.push_url)
    m.sync_trusted()
    m.sync_branch('develop')
    m.sync_branch('v2.4.x-release')
    m.cleanup()
